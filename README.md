# soyes-lease

#### 介绍
多商户租售一体商城

#### 软件架构
软件架构说明
系统分运营中心，商户中心，购物中心。运营中心和商户中心以VUE为前端框架，购物中心为uniapp。

#### 技术选型
- Spring Boot
- Spring Cloud & Alibaba Nacos
- MyBatis-Plus
- Redis/Redisson
- FastDFS
- VUE
- Uniapp

#### 部分功能截图
- 商品列表
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171236_aff5b5c7_1388129.png "商品列表.png")
- 订单列表
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171248_edef4a37_1388129.png "订单信息.png")
- 商品评论
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171300_b452d279_1388129.png "商品评论.png")
- 活动列表
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171311_4904ee09_1388129.png "活动.png")
- 优惠券
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171330_c32cdb78_1388129.png "优惠券.png")
- 首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171434_1c1c8471_1388129.jpeg "首页1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171443_9015141f_1388129.jpeg "首页2.jpg")
- 商品详情页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171453_d6f41a8c_1388129.jpeg "商品1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171501_abb9f1a0_1388129.jpeg "商品2.jpg")
- 活动页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0502/171513_af7e6e89_1388129.jpeg "活动.jpg")
- 商铺页

- 订单页

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
